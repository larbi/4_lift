package ca.polymtl.log4420
package fixture

import model.{ComiteProgramme, Cheminement}

object ProgrammeDB
{
  def multimedia = ProgrammePoly.cheminementMultimedia

  val cheminements = collection.mutable.Map.empty[ Int, Cheminement ]
  val programmes = collection.mutable.Map.empty[ ComiteProgramme, List[Cheminement] ]

  def getAllProgrammes =
  {
    programmes
  }

  def get( id: Int ) =
  {
    cheminements.get( id )
  }

  def add( cheminement: Cheminement )
  {
    cheminement.owner match {

      // Ajoute un nouveau programme de génie ( cheminement d'un comité de programme )

      case comite @ ComiteProgramme( _ ) => {

        val programme = programmes.getOrElse( comite, List.empty[Cheminement] )
        programmes( comite ) = cheminement :: programme
      }

      case _ => // Rien
    }

    cheminements( cheminement.hashCode() ) = cheminement
  }

  add( ProgrammePoly.cheminementMultimedia )
}