package ca.polymtl.log4420
package model

case class Cheminement( 
	var titre: String,
	owner: Usager,
	var sessions: List[Session],
  var coursTermine: Set[Cours]
)
{

}

case class Session( 
	periode: Periode,
	annee: Annee,
	var cours: List[Cours]
)
{
  def totalCredits = cours.map( _.credit ).sum
}

case class Annee( annee: Int )
{
  override def toString = annee.toString
}

sealed trait Usager
case class Etudiant( ) extends Usager
case class ComiteProgramme( genie: String ) extends Usager