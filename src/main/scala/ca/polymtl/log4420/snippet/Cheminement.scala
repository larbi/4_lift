package ca.polymtl.log4420
package snippet

import fixture.ProgrammeDB

// Lift
import net.liftweb._
import common.Full
import sitemap.Menu
import http.DispatchSnippet
import util.Helpers._

import xml.NodeSeq

object Cheminement
{
  // permet de convertir un url dans un cheminement
  // url: cheminement / hash
  // https://www.assembla.com/spaces/liftweb/wiki/Location_Parameters
  val menu = Menu.param[model.Cheminement](
    "Cheminement",
    "Cheminement",
    id => Full( ProgrammeDB.multimedia ), //id => asInt( id ).map( i => ProgrammeDB.get( i ) ) ,
    cheminement => cheminement.hashCode.toString
  ) / "Cheminement"

  lazy val loc = menu.toLoc
}

class Cheminement( cheminement: model.Cheminement ) extends DispatchSnippet
{
  // pour eviter de faire de la reflexion dans le html
  // ( https://www.assembla.com/spaces/liftweb/wiki/More_on_Snippets )
  def dispatch = { case "render" => render }

  def render: NodeSeq => NodeSeq =
  {
    ".session *" #> cheminement.sessions.map( session => Session( session ) ) // &
    // ".ajouter-session" #> ...
    // ".ajouter-cours" #> ...
    // etc
  }
}