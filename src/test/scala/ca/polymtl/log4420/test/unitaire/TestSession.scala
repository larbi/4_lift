package ca.polymtl.log4420.test.unitaire

import net.liftweb._
import util.StringHelpers
import common.Empty
import http.LiftSession

trait TestSession
{
  def newSession : LiftSession = new LiftSession("", StringHelpers.randomString(20), Empty)
}
